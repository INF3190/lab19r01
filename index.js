var express = require('express');
var path = require('path');//https://nodejs.org/api/path.html
var bodyParser = require('body-parser');
var app=express();
app.use(express.static(path.join(__dirname,'public')));
// create application/x-www-form-urlencoded parser
//http://expressjs.com/en/resources/middleware/body-parser.html
var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.post('/commande',urlencodedParser,function(req,res){
 res.send(req.body);
 //res.json(req.body);
});
var portNumber=3019;// au besoin changer le numero de port
app.listen(portNumber,function(){console.log(' le serveur fonctionne sur le port: '+portNumber)});
console.log('serveur demarré avec success');